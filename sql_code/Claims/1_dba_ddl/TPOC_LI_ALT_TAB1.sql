--liquibase formatted sql

--changeset asmith:TPOC_LI_ALT_TAB3_database
SET CURRENT SQLID='${SQLID}';
CREATE DATABASE DBA01TG
    BUFFERPOOL BP3
    INDEXBP    BP2
    CCSID      EBCDIC
    STOGROUP   SYSPOOL1;

--changeset asmith:TPOC_LI_ALT_TAB3_tablespace
SET CURRENT SQLID='${SQLID}';
CREATE TABLESPACE SBA01001
    IN DBA01TG
    USING STOGROUP SYSPOOL1
    PRIQTY 132480 SECQTY 13248
    ERASE  NO
    FREEPAGE 5 PCTFREE 15 FOR UPDATE 0
    GBPCACHE CHANGED
    TRACKMOD YES
    MAXPARTITIONS 20
    LOGGED
    DSSIZE 8 G
    SEGSIZE 32
    BUFFERPOOL BP0
    LOCKSIZE ANY
    LOCKMAX SYSTEM
    CLOSE NO
    COMPRESS NO
    CCSID      EBCDIC
    DEFINE YES
    MAXROWS 255
    INSERT ALGORITHM 0;
	
--changeset asmith:TPOC_LI_ALT_TAB3_table
SET CURRENT SQLID='${SQLID}';
CREATE TABLE DB2TSTE.TPOC_LI_ALT_TAB3
     (CI_ID                INTEGER NOT NULL,
      CIES_C               CHAR(2) FOR SBCS DATA NOT NULL,
      UPDT_SEQ_N           SMALLINT NOT NULL,
      SYS_STUS_C           CHAR(2) FOR SBCS DATA NOT NULL,
      CIES_EFF_DT          DATE NOT NULL,
      CIES_CAN_DT          DATE WITH DEFAULT NULL,
      LAST_MTN_DT          DATE NOT NULL,
      LAST_MTN_TM          TIME NOT NULL,
      LAST_MTN_ID          CHAR(8) FOR SBCS DATA NOT NULL,
      CONSTRAINT PRIMARY_TPOC_CBA_CIES_CI_ID
      PRIMARY KEY (CI_ID,
                   CIES_C,
                   CIES_EFF_DT),
      CONSTRAINT CI_ID0 CHECK (CI_ID > 0 ))
    IN DBA01TG.SBA01001
    PARTITION BY SIZE
    AUDIT NONE
    DATA CAPTURE CHANGES
    WITH RESTRICT ON DROP
    CCSID      EBCDIC
    NOT VOLATILE
    APPEND NO  ;
--
  COMMENT ON TABLE DB2TSTE.TPOC_LI_ALT_TAB3
    IS 'MEMBER EXTERNAL STATUS';
--
  COMMENT ON DB2TSTE.TPOC_LI_ALT_TAB3
   (LAST_MTN_ID IS 'Last Maintenance      ID',
    LAST_MTN_TM IS 'Last Maintenance      Time
                                         FORMAT: HH:MM:SS',
    LAST_MTN_DT IS 'Last Maintenance      Date
                                         FORMAT: MM/DD/YY',
    CIES_CAN_DT IS 'HICFA Cancel Date    FORMAT: MM/DD/YY',
    CIES_EFF_DT IS 'HICFA Effective       Date
                                         FORMAT: MM/DD/YY',
    SYS_STUS_C IS 'System Status Code    Indicator used to track maintenance activity on the ECS database. Action codes which imply intent: C=Committed to HST, U=Uncommitted',
    UPDT_SEQ_N IS 'Update Sequence        Number
                                         Internally generated sequence number used to track updates; part of key  on HST table.',
    CIES_C IS 'Member External Status     Code. Action Codes which imply intent: E=ESRD(End Stage Renal Disease); H=Hospice; I=Institutionalize; W=Working Aged; M=Medicaid; N=Nursing    Home; P=Personal Care
                                         Table: EXTSTAT',
    CI_ID IS 'Member Identification      Internally generated number used to identify a member');
	

--changeset asmith:TPOC_LI_ALT_TAB3_grant
SET CURRENT SQLID='${SQLID}';
GRANT SELECT ON TABLE DB2TSTE.TPOC_LI_ALT_TAB3 TO DB2TSTE;

--changeset asmith:TPOC_LI_ALT_TAB3_index
SET CURRENT SQLID='${SQLID}';
CREATE UNIQUE INDEX ${schema}.XBA0101D
    ON DB2TSTE.TPOC_LI_ALT_TAB3
     (CI_ID                 ASC,
      CIES_C                ASC,
      CIES_EFF_DT           ASC)
    USING STOGROUP SYSPOOL1
    PRIQTY 132480 SECQTY 13248
    ERASE  NO
    FREEPAGE 5 PCTFREE 15
    GBPCACHE CHANGED
    CLUSTER
    COMPRESS NO
    INCLUDE NULL KEYS
    BUFFERPOOL BP0
    CLOSE NO
    COPY NO
    DEFER NO
    DEFINE YES
    PIECESIZE 2 G;

--changeset asmith:TPOC_LI_ALT_TAB5_database
SET CURRENT SQLID='${SQLID}';
CREATE DATABASE DBA01TH
    BUFFERPOOL BP3
    INDEXBP    BP2
    CCSID      EBCDIC
    STOGROUP   SYSPOOL1;
	
--changeset asmith:TPOC_LI_ALT_TAB5_tablespace
SET CURRENT SQLID='${SQLID}';
  CREATE TABLESPACE SBA01005
    IN DBA01TH
    USING STOGROUP SYSPOOL1
    PRIQTY 132480 SECQTY 13248
    ERASE  NO
    FREEPAGE 5 PCTFREE 15 FOR UPDATE 0
    GBPCACHE CHANGED
    TRACKMOD YES
    MAXPARTITIONS 20
    LOGGED
    DSSIZE 8 G
    SEGSIZE 32
    BUFFERPOOL BP0
    LOCKSIZE ANY
    LOCKMAX SYSTEM
    CLOSE NO
    COMPRESS NO
    CCSID      EBCDIC
    DEFINE YES
    MAXROWS 255
    INSERT ALGORITHM 0;