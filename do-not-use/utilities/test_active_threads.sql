SET ECHO ON;
SET AUTOCOMMIT OFFALL;

WHENEVER SQLERROR EXIT FAILURE ROLLBACK;
SET CURRENT_SCHEMA =IBMUSER;

SPOOL C:\Users\AmySmith\Desktop\Customers\Highmark\db2_scripts\output\activethreads.sql.log;

call SYSPROC.admin_command_db2('-DIS THD(*)',11,'THD',NULL,1,0,0,0,0,0,0,NULL)

SPOOL OFF;
EXIT;
